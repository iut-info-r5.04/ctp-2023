# CTP R5.04 Automatisation de la chaine de production

## Pré-requis

Commencez par "forker" ce projet dans votre sous-groupe personnel : https://gitlab.com/univlille/iut-info/r5.04/2023/groupe-X/prenom-nom/

# Une application de gestion de fruits

Dans ce dépôt vous avez une application web utilisant le framework [Quarkus](https://quarkus.io/). Cette application enregistre des fruits dans une base de données postgres. Les fruits sont caractérisés par un `nom` et un `id`.

L'application écoute par défaut sur le port 8080. Elle est composé : 

* d'une API REST accessible sur l'URL `/fruits/` en GET, POST, PUT et DELETE 
* et d'une page HTML accessible à la racine.

Pour lancer les tests il est nécessaire d'avoir une base postgresql disponible. On la configure avec les 3 variables d'environnement suivantes : 

* QUARKUS_DATASOURCE_USERNAME=username
* QUARKUS_DATASOURCE_PASSWORD=password
* QUARKUS_DATASOURCE_REACTIVE_URL=postgresql://db:5432/fruits

La compilation et la gestion des dépendances est assurée par maven. Vous avez donc les commandes suivantes de disponible 

* `mvn test` pour lancer les tests.
* `mvn package -Dmaven.test.skip=true` pour construire l'application sans jouer les tests.
* `java -jar target/ctp2023-1.0.0-SNAPSHOT-runner.jar` pour lancer l'application en mode production.
* `mvn quarkus:dev` pour lancer l'application en mode dev.

Vous avez un Vagrantfile à disposition pour vous construire rapidement un environnement de travail.

:warning: Vous aurez peut-être besoin de faire le ménage dans vos machines virtuelles, pour ce faire, lancez la commande `VirtualBox` et supprimer les machines obsolètes. :warning:

Voici à quoi devrait ressembler votre pipeline une fois terminé.

![](./img/pipeline.png)

Les points sont indiqués à titre indicatif, ce sont des points d'efforts et pas les points de votre travail.

## Q1 - La documentation avec pandoc (3 points)

Publiez à la racine des pages gitlab le README.md et ses images uniquement pour les modifications de la branche par défaut.
Vous pouvez vérifier en visitant : https://univlille.gitlab.io/iut-info/r5.04/2023/groupe-X/preno-nom/ctp-2023/

## Q2 - Badges Pipeline (1 point)

Ajoutez le badge indiquant l'état du pipeline.

Résultat attendu : 
![pipeline](./img/badge-pipeline.png)

## Q3 - Publier un container docker (5 points)

* Après avoir construit un Dockerfile, 
* vérifié dans votre VM vagrant que le container démarre bien et permet d'avoir le service opérationnel en local, 
* ajoutez un job `docker-build` pour construire et publier le container sur la registry gitlab.

## Q4 - Lancer les tests

### Q4.1 Sans coverage ni reporting (3 points)

Ajoutez un job `junit` qui lance l'ensemble des tests unitaires, le passage des tests doit s'exécuter avant la publication du container et la publication des pages et en conditionner leur lancement.

### Q4.2 Reporting JUnit (1 points)

Ajoutez le reporting JUnit pour obtenir ça : 

![](./img/reporting-junit.png)

## Q5 - Analyse statique avec Sonar 

### Q5.1 - sans coverage (3 points)

Ajoutez l'analyse Sonar sur le serveur https://sonar.aqoba.fr en utilisant le token : `squ_776e4f6a126f456d8341972950fe32916f858122`. Comme durant le TP2, le nom de votre projet sera configuré à `nom.prenom:${project.artifactId}` grâce au paramètre `sonar.projectKey` et sera affiché dans la liste sous la forme `Nom Prénom`

N'oubliez pas d'aller vérifier sur le serveur sonar (https://sonar.aqoba.fr) avec le compte suivant `r504` / `caichai8uiphoh3ke2As`

### Q5.2 - cacher le token (2 points)

Enregistrer un token d'accès dans le dépôt git c'est une très mauvaise pratique de sécurité. Supprimez le token du code et passez par une variable cachée.

![](./img/sonar-token)

### Q5.3 - badges (1 point)

Ajoutez les badges sonar suivant :
* le nombre de bugs
* le nombre de vulnérabilités
* le nombre de bad smells

## Q6 - Code coverage

### Q6.1 - Le badge coverage (5 points)

Configurez [JaCoCo](https://www.eclemma.org/jacoco/trunk/doc/maven.html) et rajoutez le badge de code coverage.

💡 : JaCoCo était correctement configuré dans le TP2 et il est possible de forcer la génération du rapport avec la commande `verify jacoco:report`

### Q6.2 - Dans Sonar (2 point)

Remontez la couverture de test dans Sonar.

## Q7 - Versions (3 points)

À chaque pose de tag, on souhaite avoir un version gitlab (release) avec les caractéristiques suivante : 
* nom de la version = nom du tag
* description = le contenu du fichier `release-note.md`

Vérifiez en posant un nouveau tag

## Q8 - Formatage du code (3 points)

Ajoutez le plugin `fmt-maven-plugin` et configurez le job `fmt` de la façon suivante :

* Formatage du code automatique à la compilation.
* Le job `fmt` dans l'étape ̀`quality` passe en échec si le formatage du code produit un diff sur la base de code.

