package fr.univlille.iutinfo.r504;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.Matchers.equalTo;

@QuarkusTest
class FruitResourceTest {

    @Test
    public void sould_list_all_fruits() {
        given()
                .when().get("/fruits")
                .then()
                .statusCode(200)
                .body(
                        containsString("Kiwi"),
                        containsString("Durian"),
                        containsString("Pomelo"),
                        containsString("Lychee"));
    }

    @Test
    public void should_create_and_delete_fruit() {
        String location = given()
                .body(new Fruit("Cherry"))
                .when().post("/fruits")
                .getHeader("Location");

        given()
                .when().delete(location)
                .then()
                .statusCode(204);
    }

    @Test
    public void should_create_and_find_by_id() {
        String location = given().contentType(ContentType.JSON)
                .body(new Fruit("Lemon"))
                .when().post("/fruits")
                .getHeader("Location");
        given().when().get(location)
                .then().statusCode(200)
                .body(containsString("Lemon"));
    }

    @Test
    public void should_create_and_update_fruit() {
        String location = given().contentType(ContentType.JSON)
                .body(new Fruit("Pêche"))
                .when().post("/fruits")
                .getHeader("Location");
        given().contentType(ContentType.JSON).body(new Fruit("Peach"))
                .when().put(location)
                .then().statusCode(200);
        given().when().get(location)
                .then().statusCode(200)
                .body("name", equalTo("Peach"));
    }

    @Test
    public void should_read_fruit_as_object() {
        String location = given().contentType(ContentType.JSON)
                .body(new Fruit("Apricot"))
                .when().post("/fruits")
                .getHeader("Location");
        Fruit actual = given().when().get(location)
                .getBody().as(Fruit.class);
        Fruit expected = new Fruit();
        expected.name = "Apricot";
        Assertions.assertEquals(expected, actual);
    }
}
